
## Building the BoK

### Pre-requisites

The following packages are required to build the BoK.

Installation commands examples for debian family distros.

- pandoc (version >=  2.x): `apt install pandoc`
- python3
- python3 pip module: `apt install python3-pip`
- python3 gitlab package: `python3 -m pip install python-gitlab`

### Building

To build the handbook from the markdown files stored in the content directory, simply execute:

```
$ bash convert_handbook.sh
```


