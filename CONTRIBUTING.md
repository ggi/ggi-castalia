## Contributing to the GGI Body of Knowledge

We are open to contributions, and all our activity is open and public.
If you would like to participate, the best way is to register on the mailing list and join the conversation: <http://mail.ow2.org/wws/subscribe/ossgovernance>.

The Good Governance Initiative is a working group hosted at the OW2 forge:

* The [GGI Resource Centre](https://www.ow2.org/view/OSS_Governance/) contains a lot of resources and information from the beginning of the initiative.
* Activities are [elaborated and reviewed in the OW2 GitLab repository](https://gitlab.ow2.org/ggi/ggi-castalia/-/tree/dev/handbook/content) on the GitLab instance.
* Discussions happen on the [public GGI mailing list](https://mail.ow2.org/wws/info/ossgovernance), and we hold regular meetings. Minutes are redacted and stored [on this collaborative Pad](https://pad.castalia.camp/mypads/?/mypads/group/ggi-ri40n2d/view), and meetings are open to everyone.

To contribute to the GGI resource centre and GitLab activities, please follow these instructions:

* Create your OW2 user account at https://www.ow2.org/view/services/registration
* Login once at https://www.ow2.org/
* To edit the [Resource Center](https://www.ow2.org/view/OSS_Governance/): send us your username, and we'll grant you appropriate access.
* To access the GitLab group: login once to https://gitlab.ow2.org with your OW2 credentials, then [let us know](mailto:ossgovernance@ow2.org) when it's done, and we'll grant you access to the GGI group in GitLab.
* To access our Rocket.Chat service you need to:
  - Login at least once on https://gitlab.ow2.org using your OW2 credentials.
  - Open Rocket.Chat and choose a Rocket.Chat username when prompted.
  - Go to the #general channel and ask for GGI channel access there.
  - Once access is granted, you should be able to access #good-governance channel.

